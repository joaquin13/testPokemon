//
//  DetailView.swift
//  test
//
//  Created by Macbook on 11/22/20.
//  Copyright © 2020 Macbook. All rights reserved.
//

import Foundation
import UIKit

class DetailView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    // MARK: Properties
    var presenter: DetailPresenterProtocol?
    var delegate : TypeProtocol?
    @IBOutlet weak var tableDetail: UITableView!
    var data : Pokemon?
    var backgroundType : UIColor?

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        self.presenter?.viewDidLoad()
    }
}

extension DetailView{
    func prepareUI(){
        //TypesPokemonCell
        let nameCell = "NameCell"
        let nameNib = UINib(nibName: nameCell, bundle: nil)
        self.tableDetail.register(nameNib, forCellReuseIdentifier: nameCell)
        let imagesCell = "ImagesDetailCell"
        let imagesNib = UINib(nibName: imagesCell, bundle: nil)
        self.tableDetail.register(imagesNib, forCellReuseIdentifier: imagesCell)
        let typesCell = "TypesPokemonCell"
        let typesNib = UINib(nibName: typesCell, bundle: nil)
        self.tableDetail.register(typesNib, forCellReuseIdentifier: typesCell)
        self.tableDetail.delegate = self
        self.tableDetail.dataSource = self
    }
}

extension DetailView{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let _ = self.data, let stat = self.data?.stats?.count, let types = self.data?.types?.count   else {return 0}
        return 3 + (types + 1) + (stat + 1)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let _ = self.data, let typesCount = self.data?.types?.count   else {return UITableViewCell()}
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as!  NameCell
            guard let name = self.data?.name , let order = self.data?.order   else {return UITableViewCell()}
            cell.name = name.capitalized + "\n" + "#\(order)"
            cell.flagPrincipal = true
            cell.sizeFont = 25
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesDetailCell") as!  ImagesDetailCell
            cell.url = self.data?.sprites?.front_default
            cell.typePokemon = "Default"
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ImagesDetailCell") as!  ImagesDetailCell
            cell.url = self.data?.sprites?.front_shiny
            cell.typePokemon = "Shiny"
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as!  NameCell
            cell.name = "Tipos:"
            cell.flagPrincipal = true
            cell.sizeFont = 17
            return cell
        }else if indexPath.row > 3  && indexPath.row < typesCount + 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as!  NameCell
            guard let nameType = self.data?.types?[indexPath.row - 4].type?.name   else {return UITableViewCell()}
            let colorCell = self.getColorBackground (typePokemon : nameType)
            cell.name = "-" + nameType
            cell.backGroundCell = colorCell
            return cell
        }else if indexPath.row == typesCount + 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as!  NameCell
            cell.name = "Stat:"
            cell.flagPrincipal = true
            cell.sizeFont = 17
            return cell
        }else if indexPath.row > typesCount + 4 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as!  NameCell
            guard let nameStat = self.data?.stats?[indexPath.row - (typesCount + 5)].stat?.name, let baseStat = self.data?.stats?[indexPath.row - (typesCount + 5)].base_stat   else {return UITableViewCell()}
            cell.name = "-" + nameStat + " : " + "\(baseStat)"
            return cell
        }else{
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let _ = self.data, let typesCount = self.data?.types?.count   else {return 0}
        
        if indexPath.row == 0{
            return 80
        }else if indexPath.row == 1{
            return 230
        }else if indexPath.row == 2{
            return 230
        }else if indexPath.row == 3{
            return 40
        }else if indexPath.row > 3  && indexPath.row < typesCount + 4 {
            return 33
        }else if indexPath.row == typesCount + 4 {
            return 40
        }else if indexPath.row > typesCount + 4 {
            return 33
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let typesCount = self.data?.types?.count  else {return}
        if indexPath.row > 3  && indexPath.row < typesCount + 4 {
            guard let nameType = self.data?.types?[indexPath.row - 4].type?.name   else {return}
            self.delegate?.selectTypeProtocol(typePokemon: nameType)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

extension DetailView: DetailViewProtocol {
    func showDetail(data:Pokemon){
        self.data = data
        self.tableDetail.reloadData()
    }
}

extension DetailView {
    func getColorBackground (typePokemon : String) -> UIColor{
        switch typePokemon {
            case "normal":
                return .backgroundNormal
            case "fire":
            return .backgroundFire
            case "water":
            return .backgroundWater
            case "grass":
            return .backgroundGrass
            case "electric":
            return .backgroundElectric
            case "ice":
            return .backgroundIce
            case "fighting":
            return .backgroundFighting
            case "poison":
            return .backgroundPoison
            case "ground":
            return .backgroundGround
            case "flying":
            return .backgroundFlying
            case "psychic":
            return .backgroundPsychic
            case "bug":
            return .backgroundBug
            case "rock":
            return .backgroundRock
            case "ghost":
            return .backgroundGhost
            case "dark":
            return .backgroundDark
            case "dragon":
            return .backgroundDragon
            case "steel":
            return .backgroundSteel
            case "fairy":
            return .backgroundFairy
            default:
            return .white
        }
        
    }
}
