//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation
import UIKit

class ListWireFrame: ListWireFrameProtocol {

    class func createListModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "NavigationListView")
        if let view = navController.children.first as? ListView {
            let presenter: ListPresenterProtocol & ListInteractorOutputProtocol = ListPresenter()
            let interactor: ListInteractorInputProtocol & ListRemoteDataManagerOutputProtocol = ListInteractor()
            let localDataManager: ListLocalDataManagerInputProtocol = ListLocalDataManager()
            let remoteDataManager: ListRemoteDataManagerInputProtocol = ListRemoteDataManager()
            let wireFrame: ListWireFrameProtocol = ListWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            presenter.interactor = interactor
            interactor.presenter = presenter
            interactor.localDatamanager = localDataManager
            interactor.remoteDatamanager = remoteDataManager
            remoteDataManager.remoteRequestHandler = interactor
            
            return navController
        }
        return UIViewController()
    }
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "ListView", bundle: Bundle.main)
    }
    
    
    func presentNextViewDetalle(from view : ListViewProtocol, object: Pokemon, delegate: TypeProtocol){
        let newViewDetalle = DetailWireFrame.createDetailModule(data : object, delegate: delegate )
        if let sourceView = view as? UIViewController{
            sourceView.navigationController?.pushViewController(newViewDetalle, animated: true)
        }
    }
    
}
