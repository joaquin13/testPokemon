//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation
import UIKit

class ListView: UIViewController {
    
    @IBOutlet weak var tablePokemon: UITableView!
    @IBOutlet weak var loadIndicator: UIActivityIndicatorView!
    @IBOutlet weak var textFieldSearch: UITextField!
    var arrayPokemon = [Pokemon]()
    var countPokemon = 0
    var arrayPokemonTypes = [Pokemon]()
    var countPokemonTypes = 0
    var flagPokemonTypes = false
    
    // MARK: Properties
    var presenter: ListPresenterProtocol?
    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUI()
        self.presenter?.viewDidLoad()
        self.presenter?.getPokemonList(indexTop : self.countPokemon)
    }
    
}

extension ListView{
    func prepareUI(){
        let tap = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        self.textFieldSearch.delegate = self
        let nameCell = "PokemonCell"
        let nib = UINib(nibName: nameCell, bundle: nil)
        self.tablePokemon.register(nib, forCellReuseIdentifier: nameCell)
        let nameCellFooter = "FooterCell"
        let nibFooter = UINib(nibName: nameCellFooter, bundle: nil)
        self.tablePokemon.register(nibFooter, forCellReuseIdentifier: nameCellFooter)
        self.tablePokemon.delegate = self
        self.tablePokemon.dataSource = self
        self.countPokemon = 20
    }
    
    @IBAction func actionSearch (_ sender: UIButton) {
        guard let textSearch = self.textFieldSearch.text else {return}
        self.presenter?.viewDidLoad()
        self.presenter?.getPokemon(nameOrId: textSearch)
    }
}

extension ListView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if flagPokemonTypes {
            if self.countPokemonTypes > self.arrayPokemonTypes.count{
                return (20 - (self.countPokemonTypes - self.arrayPokemonTypes.count)) + 1
            }else{
                return 21
            }
        }else{
            return (self.arrayPokemon.count < 2 ) ? self.arrayPokemon.count : self.arrayPokemon.count + 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if flagPokemonTypes {
            var numCells = 0
            if self.countPokemonTypes > self.arrayPokemonTypes.count{
                numCells = (20 - (self.countPokemonTypes - self.arrayPokemonTypes.count)) + 1
            }else{
                numCells = 21
            }
            if indexPath.row < (numCells - 1) {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonCell") as!  PokemonCell
                cell.pokemon = self.arrayPokemonTypes[ (self.countPokemonTypes - 20) + indexPath.row]
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as!  FooterCell
                cell.protocolFlow = self
                cell.flagEnableBefore = (self.countPokemonTypes == 20) ? true : false
                cell.flagEnableNext = (self.countPokemonTypes > self.arrayPokemonTypes.count) ? true : false
                return cell
            }
        }else{
            if indexPath.row < 20 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "PokemonCell") as!  PokemonCell
                cell.pokemon = self.arrayPokemon[indexPath.row]
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "FooterCell") as!  FooterCell
                cell.protocolFlow = self
                cell.flagEnableBefore = (self.countPokemon == 20) ? true : false
                return cell
            }
        }
        
        
        
    }
}

extension ListView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row < 20 {
            return 100
        }else{
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if flagPokemonTypes {
            if indexPath.row != 20 {
                self.presenter?.getDataNextViewDetalle(object: self.arrayPokemonTypes[ (self.countPokemonTypes - 20) + indexPath.row], delegate: self)
            }
        }else{
            if indexPath.row != 20 {
                self.presenter?.getDataNextViewDetalle(object: self.arrayPokemon[indexPath.row], delegate: self)
            }
            
        }
    }
}

extension ListView: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldTextString = textField.text else {
            return true
        }
        let oldText = oldTextString as NSString
        let newString = oldText.replacingCharacters(in: range, with: string) as NSString
        if newString.isEqual(to: "") {
            self.flagPokemonTypes = false
            self.tablePokemon.reloadData()
        }
        return true
    }
}



extension ListView: FlowProtocol {
    func nextFlow() {
        if flagPokemonTypes {
            self.countPokemonTypes += 20
            self.tablePokemon.reloadData()
            let topIndex = IndexPath(row: 0, section: 0)
            self.tablePokemon.scrollToRow(at: topIndex, at: .top, animated: true)
        }else{
            self.countPokemon += 20
            self.reloadPokemons()
        }
    }
    
    func beforeFlow() {
        if flagPokemonTypes {
            self.countPokemonTypes -= 20
            self.tablePokemon.reloadData()
            let topIndex = IndexPath(row: 0, section: 0)
            self.tablePokemon.scrollToRow(at: topIndex, at: .top, animated: true)
        }else{
            self.countPokemon -= 20
            self.reloadPokemons()
        }
    }
    
    func reloadPokemons(){
        self.presenter?.viewDidLoad()
        self.presenter?.getPokemonList(indexTop : self.countPokemon)
        let topIndex = IndexPath(row: 0, section: 0)
        self.tablePokemon.scrollToRow(at: topIndex, at: .top, animated: true)
    }
}

extension ListView: ListViewProtocol {
    
    func turnOnTypeFlag() {
        self.flagPokemonTypes = true
    }
    
    func turnOffTypeFlag() {
        self.flagPokemonTypes = false
    }
    
    // TODO: implement view output methods
    func callBackData() {
        
    }
    
    func iniciar() {
        DispatchQueue.main.async {
            self.loadIndicator.isHidden = false
            self.loadIndicator.startAnimating()
        }
    }
    
    func detener() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.loadIndicator.stopAnimating()
            self.loadIndicator.isHidden = true
            self.tablePokemon.reloadData()
        }
    }
    
    func returnPokemonList(arrayPokemon : [Pokemon]){
        if arrayPokemon.isEmpty {
            let alert = UIAlertController(title: "Lo sentimos", message: "Tu busqueda no obtuvo ninguna coincidencia.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }else if flagPokemonTypes {
            self.arrayPokemonTypes = arrayPokemon
            self.countPokemonTypes = 20
            self.tablePokemon.reloadData()
            let topIndex = IndexPath(row: 0, section: 0)
            self.tablePokemon.scrollToRow(at: topIndex, at: .top, animated: true)
            
        }else{
            if arrayPokemon.count == 1 {
                self.presenter?.getDataNextViewDetalle(object: arrayPokemon.first!, delegate: self)
            }else{
                self.arrayPokemon = arrayPokemon
            }
        }
    }
}

extension ListView : TypeProtocol {
    func selectTypeProtocol(typePokemon: String) {
        self.presenter?.viewDidLoad()
        self.presenter?.getPokemon(nameOrId: typePokemon)
        self.textFieldSearch.text = typePokemon
        print(typePokemon)
    }
}

