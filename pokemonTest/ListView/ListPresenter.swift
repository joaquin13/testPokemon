//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation

class ListPresenter  {
    
    var stringCollection = [String]()
    
    // MARK: Properties
    weak var view: ListViewProtocol?
    var interactor: ListInteractorInputProtocol?
    var wireFrame: ListWireFrameProtocol?
    
}

extension ListPresenter: ListPresenterProtocol {
    func getPokemon(nameOrId: String) {
        interactor?.getPokemon(nameOrId: nameOrId)
    }
    
    func getDataNextViewDetalle(object: Pokemon, delegate : TypeProtocol) {
        guard let view = self.view else { return}
        wireFrame?.presentNextViewDetalle(from: view, object: object, delegate: delegate)
    }
    
    // TODO: implement presenter methods
    func viewDidLoad() {
        //Se consulta al interactor para extraer los datos remotos
        view?.iniciar()
    }
    
    func getPokemonList(indexTop : Int){
        interactor?.getPokemonList(indexTop : indexTop)
        
    }
    
}

extension ListPresenter: ListInteractorOutputProtocol {
    
    func turnOnTypeFlag() {
        view?.turnOnTypeFlag()
    }
    
    func turnOffTypeFlag() {
        view?.turnOffTypeFlag()
    }
    
    // TODO: implement interactor output methods
    func callBackData() {
        view?.callBackData()
    }
    
    func returnPokemonList(arrayPokemon : [Pokemon]){
        view?.returnPokemonList(arrayPokemon: arrayPokemon)
        view?.detener()
    }
}
