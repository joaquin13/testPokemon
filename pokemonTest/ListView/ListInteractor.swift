//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation

class ListInteractor: ListInteractorInputProtocol {
    
    // MARK: Properties
    weak var presenter: ListInteractorOutputProtocol?
    var localDatamanager: ListLocalDataManagerInputProtocol?
    var remoteDatamanager: ListRemoteDataManagerInputProtocol?
    
    func getExternalData() {
        remoteDatamanager?.externalGetData()
    }
    
    func getPokemonList(indexTop : Int){
        remoteDatamanager?.getPokemonList(indexTop : indexTop)
        presenter?.turnOffTypeFlag()
    }
    
    func getPokemon(nameOrId: String) {
        if evaluateString(cad : nameOrId.trimmingCharacters(in: .whitespacesAndNewlines).lowercased()) {
            remoteDatamanager?.getPokemonByType(typePokemon: nameOrId.trimmingCharacters(in: .whitespacesAndNewlines).lowercased())
            presenter?.turnOnTypeFlag()
        }else{
            remoteDatamanager?.getPokemon(nameOrId: nameOrId.trimmingCharacters(in: .whitespacesAndNewlines).lowercased())
            presenter?.turnOffTypeFlag()
        }
    }
    
    func evaluateString(cad : String) -> Bool{
        if cad.elementsEqual("normal") || cad.elementsEqual("fire") || cad.elementsEqual("water")
            || cad.elementsEqual("grass") || cad.elementsEqual("electric") || cad.elementsEqual("ice")
            || cad.elementsEqual("fighting") || cad.elementsEqual("poison") || cad.elementsEqual("ground")
            || cad.elementsEqual("flying") || cad.elementsEqual("psychic") || cad.elementsEqual("bug")
            || cad.elementsEqual("rock") || cad.elementsEqual("ghost") || cad.elementsEqual("dark")
            || cad.elementsEqual("dragon") || cad.elementsEqual("steel") || cad.elementsEqual("fairy"){
            return true
        }
        return false
    }

}

extension ListInteractor: ListRemoteDataManagerOutputProtocol {
    
    // TODO: Implement use case methods
    func callBackData(){
        presenter?.callBackData()
    }
    
    func returnPokemonList(arrayPokemon : [Pokemon]){
        
        var arraySort = arrayPokemon
        arraySort.sort {
            $0.id! < $1.id!
        }
        presenter?.returnPokemonList(arrayPokemon: arraySort)
        
    }
    
    
    
}

