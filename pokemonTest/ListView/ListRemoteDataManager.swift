//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//


import Foundation
import Alamofire

class ListRemoteDataManager:ListRemoteDataManagerInputProtocol {
    
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol?
    var arrayPokemon = [Pokemon]()
    var arrayLinks = [String]()
    
    func externalGetData() {
        
    }
    
    func getPokemonList(indexTop : Int){
        self.arrayPokemon.removeAll()
        for i in (indexTop - 19)...indexTop {
            AF.request("https://pokeapi.co/api/v2/pokemon/" + "\(i)" + "/", method: .get).responseDecodable(of: Pokemon.self) { response in
                guard let objPokemon = response.value else { return }
                self.arrayPokemon.append(objPokemon)
                if self.arrayPokemon.count == 20{
                    self.remoteRequestHandler?.returnPokemonList(arrayPokemon: self.arrayPokemon)
                }
            }
        }
    }
    
    func getPokemon(nameOrId: String) {
        self.arrayPokemon.removeAll()
        AF.request("https://pokeapi.co/api/v2/pokemon/" + nameOrId + "/", method: .get).responseDecodable(of: Pokemon.self) { response in
            guard let objPokemon = response.value else {  self.remoteRequestHandler?.returnPokemonList(arrayPokemon: self.arrayPokemon); return }
            self.arrayPokemon.append(objPokemon)
            self.remoteRequestHandler?.returnPokemonList(arrayPokemon: self.arrayPokemon)
        }
    }
    
    func getPokemonByType(typePokemon: String) {
        self.arrayLinks.removeAll()
        AF.request("https://pokeapi.co/api/v2/type/" + typePokemon + "/", method: .get).responseDecodable(of: PokemonUrl.self) { response in
            guard let objPokemon = response.value, let arrayObj = objPokemon.pokemon else { return }
            for obj in arrayObj{
                if let url = obj.pokemon?.url{
                    self.arrayLinks.append(url)
                }
            }
            self.getPokemonFromUrl(arrayLinks : self.arrayLinks)
        }
    }
    
    func getPokemonFromUrl (arrayLinks : [String]) {
        self.arrayPokemon.removeAll()
        for urlItem in arrayLinks{
            AF.request(urlItem, method: .get).responseDecodable(of: Pokemon.self) { response in
                guard let objPokemon = response.value else { return }
                self.arrayPokemon.append(objPokemon)
                if self.arrayPokemon.count == arrayLinks.count{
                    self.remoteRequestHandler?.returnPokemonList(arrayPokemon: self.arrayPokemon)
                }
            }
        }
    }
}
