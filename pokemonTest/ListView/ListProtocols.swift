//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation
import UIKit

protocol ListViewProtocol: AnyObject {
    // PRESENTER -> VIEW
    
    var presenter: ListPresenterProtocol? { get set }
    func callBackData()
    func iniciar()
    func detener()
    func returnPokemonList(arrayPokemon : [Pokemon])
    func turnOnTypeFlag()
    func turnOffTypeFlag()
}

protocol ListWireFrameProtocol: AnyObject {
    // PRESENTER -> WIREFRAME
    static func createListModule() -> UIViewController
    func presentNextViewDetalle(from view : ListViewProtocol, object: Pokemon, delegate : TypeProtocol)
    
}

protocol ListPresenterProtocol: AnyObject {
    // VIEW -> PRESENTER
    var view: ListViewProtocol? { get set }
    var interactor: ListInteractorInputProtocol? { get set }
    var wireFrame: ListWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func getPokemonList(indexTop : Int)
    func getPokemon(nameOrId : String)
    func getDataNextViewDetalle(object: Pokemon, delegate : TypeProtocol)
    
}

protocol ListInteractorOutputProtocol: AnyObject {
// INTERACTOR -> PRESENTER
    func callBackData()
    func returnPokemonList(arrayPokemon : [Pokemon])
    func turnOnTypeFlag()
    func turnOffTypeFlag()
}

protocol ListInteractorInputProtocol: AnyObject {
    // PRESENTER -> INTERACTOR
    var presenter: ListInteractorOutputProtocol? { get set }
    var localDatamanager: ListLocalDataManagerInputProtocol? { get set }
    var remoteDatamanager: ListRemoteDataManagerInputProtocol? { get set }
    //
    func getExternalData()
    func getPokemonList(indexTop : Int)
    func getPokemon(nameOrId : String)
    
}

protocol ListDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> DATAMANAGER
}

protocol ListRemoteDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> REMOTEDATAMANAGER
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol? { get set }
    func externalGetData()
    func getPokemonList(indexTop : Int)
    func getPokemon(nameOrId : String)
    func getPokemonByType(typePokemon : String)
    
}

protocol ListRemoteDataManagerOutputProtocol: AnyObject {
    // REMOTEDATAMANAGER -> INTERACTOR
    func callBackData()
    func returnPokemonList(arrayPokemon : [Pokemon])
}

protocol ListLocalDataManagerInputProtocol: AnyObject {
    // INTERACTOR -> LOCALDATAMANAGER
}
