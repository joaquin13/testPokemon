//
//  SceneDelegate.swift
//  pokemonTest
//
//  Created by Joaquin on 27/05/22.
//

import Foundation


struct Pokemon: Codable {
    var id : Int?
    var name: String?
    var order: Int?
    var sprites: Sprites?
    var types: [Types]?
    var stats: [Stats]?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case order = "order"
        case sprites = "sprites"
        case types = "types"
        case stats = "stats"
    }
}

struct Sprites: Codable {
    var front_default: String?
    var front_shiny: String?
    
    enum CodingKeys: String, CodingKey {
        case front_default = "front_default"
        case front_shiny = "front_shiny"
    }
}

struct Stats: Codable {
    var base_stat: Int?
    var effort: Int?
    var stat: ModelStat?
    
    enum CodingKeys: String, CodingKey {
        case base_stat = "base_stat"
        case effort = "effort"
        case stat = "stat"
    }
}

struct ModelStat: Codable {
    var name: String?
    var url: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
    }
}

struct Types: Codable {
    var slot: Int?
    var type: ModelType?
    
    enum CodingKeys: String, CodingKey {
        case slot = "slot"
        case type = "type"
    }
}

struct ModelType: Codable {
    var name: String?
    var url: String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
    }
}

struct PokemonUrl: Codable {
    var pokemon: [PokemonByType]?
    
    enum CodingKeys: String, CodingKey {
        case pokemon = "pokemon"
    }
}

struct PokemonByType: Codable {
    var pokemon: ModelType?
    
    enum CodingKeys: String, CodingKey {
        case pokemon = "pokemon"
    }
}


/*
 {
       "pokemon": {
         "name": "bulbasaur",
         "url": "https://pokeapi.co/api/v2/pokemon/1/"
       },
       "slot": 2
     }
*/
