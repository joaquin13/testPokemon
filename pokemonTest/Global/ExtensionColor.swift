//
//  ExtensionColor.swift
//  pokemonTest
//
//  Created by Joaquin on 29/05/22.
//

import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    //168,168,120,255
    
    class var backgroundNormal: UIColor {
        return UIColor(red: 168.0 / 255.0, green: 168.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(240,128,48,255)
    
    class var backgroundFire: UIColor {
        return UIColor(red: 240.0 / 255.0, green: 128.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(104,144,240,255)
    
    class var backgroundWater: UIColor {
        return UIColor(red: 104.0 / 255.0, green: 144.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(120,200,79,255)
    class var backgroundGrass: UIColor {
        return UIColor(red: 120.0 / 255.0, green: 200.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(248,208,48,255)
    class var backgroundElectric: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 208.0 / 255.0, blue: 48.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(152,216,216,255)
    class var backgroundIce: UIColor {
        return UIColor(red: 152.0 / 255.0, green: 216.0 / 255.0, blue: 216.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(192,48,40,255)
    class var backgroundFighting: UIColor {
        return UIColor(red: 192.0 / 255.0, green: 48.0 / 255.0, blue: 40.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(224,192,104,255)
    class var backgroundGround: UIColor {
        return UIColor(red: 224.0 / 255.0, green: 192.0 / 255.0, blue: 104.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(159,64,159,255)
    class var backgroundPoison: UIColor {
        return UIColor(red: 159.0 / 255.0, green: 64.0 / 255.0, blue: 159.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(167,144,240,255)
    class var backgroundFlying: UIColor {
        return UIColor(red: 167.0 / 255.0, green: 144.0 / 255.0, blue: 240.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(248,88,136,255)
    class var backgroundPsychic: UIColor {
        return UIColor(red: 248.0 / 255.0, green: 88.0 / 255.0, blue: 136.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(167,184,32,255)
    class var backgroundBug: UIColor {
        return UIColor(red: 167.0 / 255.0, green: 184.0 / 255.0, blue: 32.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(185,160,56,255)
    class var backgroundRock: UIColor {
        return UIColor(red: 185.0 / 255.0, green: 160.0 / 255.0, blue: 56.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(112,87,152,255)
    class var backgroundGhost: UIColor {
        return UIColor(red: 112.0 / 255.0, green: 87.0 / 255.0, blue: 152.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(111,88,72,255)
    class var backgroundDark: UIColor {
        return UIColor(red: 111.0 / 255.0, green: 88.0 / 255.0, blue: 72.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(112,56,248,255)
    class var backgroundDragon: UIColor {
        return UIColor(red: 112.0 / 255.0, green: 56.0 / 255.0, blue: 248.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(184,184,208,255)
    class var backgroundSteel: UIColor {
        return UIColor(red: 184.0 / 255.0, green: 184.0 / 255.0, blue: 208.0 / 255.0, alpha: 1.0)
    }
    
    //rgba(240,182,188,255)
    class var backgroundFairy: UIColor {
        return UIColor(red: 240.0 / 255.0, green: 182.0 / 255.0, blue: 188.0 / 255.0, alpha: 1.0)
    }
}
