//
//  FooterCell.swift
//  pokemonTest
//
//  Created by Joaquin on 29/05/22.
//

import Foundation
import UIKit

class FooterCell : UITableViewCell{
    
    @IBOutlet weak var beforeLabel: UILabel!
    @IBOutlet weak var beforeImage: UIImageView!
    @IBOutlet weak var nextLabel: UILabel!
    @IBOutlet weak var nextImage: UIImageView!
    var protocolFlow : FlowProtocol?
    var flagEnableBefore = false
    var flagEnableNext = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGestureBefore1 = UITapGestureRecognizer(target: self, action: #selector(actionBeforeFlow))
        self.beforeImage.isUserInteractionEnabled = true
        self.beforeImage.addGestureRecognizer(tapGestureBefore1)
        let tapGestureBefore2 = UITapGestureRecognizer(target: self, action: #selector(actionBeforeFlow))
        self.beforeLabel.isUserInteractionEnabled = true
        self.beforeLabel.addGestureRecognizer(tapGestureBefore2)
        
        
        let tapGestureNext1 = UITapGestureRecognizer(target: self, action: #selector(actionNextFlow))
        self.nextImage.isUserInteractionEnabled = true
        self.nextImage.addGestureRecognizer(tapGestureNext1)
        let tapGestureNext2 = UITapGestureRecognizer(target: self, action: #selector(actionNextFlow))
        self.nextLabel.isUserInteractionEnabled = true
        self.nextLabel.addGestureRecognizer(tapGestureNext2)
      
    }
    
    override func layoutSubviews() {
        self.beforeImage.isHidden = self.flagEnableBefore
        self.beforeLabel.isHidden = self.flagEnableBefore
        
        self.nextImage.isHidden = self.flagEnableNext
        self.nextLabel.isHidden = self.flagEnableNext
    }
    
    @objc func actionBeforeFlow(){
        guard let objProtocol = self.protocolFlow else {return}
        objProtocol.beforeFlow()
    }
    
    @objc func actionNextFlow(){
        guard let objProtocol = self.protocolFlow else {return}
        objProtocol.nextFlow()
    }
    
}
