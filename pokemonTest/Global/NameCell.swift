//
//  PokemonCell.swift
//  pokemonTest
//
//  Created by Joaquin on 28/05/22.
//

import Foundation
import UIKit

class NameCell : UITableViewCell{
    
    @IBOutlet weak var nameLabel: UILabel!
    var name : String?
    var flagPrincipal = false
    var sizeFont = 0
    var backGroundCell : UIColor = .white
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        guard let name = self.name else {return}
        self.nameLabel.text = name
        self.contentView.backgroundColor = self.backGroundCell
        if flagPrincipal {
            self.nameLabel.font = UIFont.boldSystemFont(ofSize: CGFloat(self.sizeFont))
        }
    }
    
}
