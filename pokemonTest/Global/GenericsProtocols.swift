//
//  GenericsProtocols.swift
//  pokemonTest
//
//  Created by Joaquin on 29/05/22.
//

import Foundation

protocol FlowProtocol {
    func nextFlow()
    func beforeFlow()
}

protocol TypeProtocol {
    func selectTypeProtocol(typePokemon : String)
}
