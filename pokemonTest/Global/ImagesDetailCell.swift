//
//  PokemonCell.swift
//  pokemonTest
//
//  Created by Joaquin on 28/05/22.
//

import Foundation
import UIKit

class ImagesDetailCell : UITableViewCell{
    
    
    @IBOutlet weak var pokemonImage: UIImageView!
    @IBOutlet weak var labelType: UILabel!
    var url : String?
    var typePokemon : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func layoutSubviews() {
        self.labelType.text = typePokemon ?? ""
        if let url = URL(string: self.url!) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    self.pokemonImage.image = UIImage(data: data)
                }
            }
            task.resume()
        }
    }
    
}
