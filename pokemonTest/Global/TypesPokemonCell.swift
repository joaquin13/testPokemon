//
//  TypesPokemonCell.swift
//  pokemonTest
//
//  Created by Joaquin on 29/05/22.
//

import Foundation
import UIKit

class TypesPokemonCell : UITableViewCell{
    
    @IBOutlet weak var typeLabel: UILabel!
    var types: [Types]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        var typesText = "Tipos:\n"
        guard let types = self.types else {return}
        for itemType in types{
            guard let tipo = itemType.type?.name else {return}
            typesText += (tipo + "\n")
        }
        self.typeLabel.text = typesText
    }
    
}
