//
//  PokemonCell.swift
//  pokemonTest
//
//  Created by Joaquin on 28/05/22.
//

import Foundation
import UIKit

class PokemonCell : UITableViewCell{
    
    @IBOutlet weak var viewBodyCard: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imagePokemon: UIImageView!
    var pokemon : Pokemon?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        guard let objPokemon = self.pokemon, let namePokemon = objPokemon.name, let frontSprite = objPokemon.sprites?.front_default else {return}
        viewBodyCard.layer.cornerRadius = 8
        viewBodyCard.layer.masksToBounds = false
        viewBodyCard.layer.shadowColor = UIColor.black.cgColor
        viewBodyCard.layer.shadowOffset = CGSize(width: 0, height: 1);
        viewBodyCard.layer.shadowOpacity =  0.3
        viewBodyCard.backgroundColor = .white
        self.nameLabel.text = namePokemon
        if let url = URL(string: frontSprite) {
            let task = URLSession.shared.dataTask(with: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async {
                    self.imagePokemon.image = UIImage(data: data)
                }
            }
            task.resume()
        }
        
    }
    
}
